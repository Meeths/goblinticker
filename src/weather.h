#pragma once

#include <HTTPClient.h>

class Weather
{
    public:
        String FetchData(HTTPClient& httpClient, String province, String city);
};