#include "ticker.h"

void Ticker::Init()
{
    mLedMatrix.init();
    mLedMatrix.setIntensity(1); // range is 0-15
    mLedMatrix.clear();
    mLedMatrix.commit();
}

void Ticker::NextMessage()
{
    mCurrentMessageIndex = (mCurrentMessageIndex+1) % MAX_MESSAGE_LIST_SIZE;

    if(!mMessageList[mTopMessageIndex].mScroll)
        mLedMatrix.FitText();

    mLedMatrix.clear();
    mLedMatrix.setText(mMessageList[mTopMessageIndex].mMessage);

}

void Ticker::PushMessage(char* message, unsigned int displayTime, bool scroll)
{
    mMessageList[mTopMessageIndex].mScroll = scroll;
    mMessageList[mTopMessageIndex].mDisplayTime = displayTime;
    strcpy(mMessageList[mTopMessageIndex].mMessage, message);
    mTopMessageIndex = (mTopMessageIndex+1) % MAX_MESSAGE_LIST_SIZE;
}

void Ticker::SetMessage(const char* message)
{
    mLedMatrix.setText(message);
}

void Ticker::Update(unsigned int deltams)
{
    mLedMatrix.clear();

    if(mUseMessageQueue)
    {
        if(!mMessageList[mCurrentMessageIndex].mScroll)
        {
            mMessageList[mCurrentMessageIndex].mDisplayTime -= deltams;
            if(mMessageList[mCurrentMessageIndex].mDisplayTime < 0)
                NextMessage();
        }
        else
        {
            mLedMatrix.scrollTextLeft();
        //  if(mLedMatrix.IsEndOfScroll())
        //      NextMessage();
        }
    }
    else
    {
        mLedMatrix.scrollTextLeft();
    }

    
    mLedMatrix.drawText();
    mLedMatrix.commit();

}