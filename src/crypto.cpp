#include "crypto.h"
#include <ArduinoJson.h>

#define CRYPTO_VALUE_API_URL "https://min-api.cryptocompare.com/data/price?tsyms=USD&fsym="
#define CRYPTO_1DAGGREGATE_API_URL "https://min-api.cryptocompare.com/data/histoday?tsym=USD&limit=1&aggregate=1&fsym="

float GetCurrentCryptoPrice(HTTPClient& httpClient, const String& symbol)
{
    float usdPrice = 0;
    httpClient.begin(CRYPTO_VALUE_API_URL + symbol); //HTTP
    int httpCode = httpClient.GET();
    if (httpCode > 0) {
      if (httpCode == HTTP_CODE_OK) {
        String jsonPayload = httpClient.getString();
        DynamicJsonDocument doc(4096);
        deserializeJson(doc, jsonPayload);
        JsonObject obj = doc.as<JsonObject>();
        usdPrice = obj["USD"].as<float>();
      }
    }
    httpClient.end();

    return usdPrice;
}

float GetOpeningCryptoPrice(HTTPClient& httpClient, const String& symbol)
{
    float usdPrice = 0;
    httpClient.begin(CRYPTO_1DAGGREGATE_API_URL + symbol); //HTTP
    int httpCode = httpClient.GET();
    if (httpCode > 0) {
      if (httpCode == HTTP_CODE_OK) {
        String jsonPayload = httpClient.getString();
        DynamicJsonDocument doc(4096);
        deserializeJson(doc, jsonPayload);
        JsonObject obj = doc.as<JsonObject>();
        usdPrice = obj["Data"][1]["open"].as<float>();
      }
    }
    httpClient.end();

    return usdPrice;
}


String Crypto::FetchData(HTTPClient& httpClient, String symbol)
{
    float currentPrice = GetCurrentCryptoPrice(httpClient, symbol);
    float openingPrice = GetOpeningCryptoPrice(httpClient, symbol);
    if(currentPrice == 0 || openingPrice == 0)
      return "Error fetching " + symbol + " price";

    float percentChange = ((currentPrice / openingPrice) - 1) * 100;
    String tickString = 
    "$" + symbol + ":" +
    String(currentPrice, 2) + 
    (percentChange < 0 ? " (" : " (+")+
    String(percentChange, 2) + "%)";

    return tickString;
}