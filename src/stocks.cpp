#include "stocks.h"
#include <ArduinoJson.h>

#define STOCKS_API_URL "https://query1.finance.yahoo.com/v7/finance/quote?symbols="

String Stocks::FetchData(HTTPClient& httpClient, String symbol)
{
    httpClient.begin(STOCKS_API_URL + symbol); //HTTP

    int httpCode = httpClient.GET();
    String retVal;
    if (httpCode > 0) {
      if (httpCode == HTTP_CODE_OK) {
        String jsonPayload = httpClient.getString();
        DynamicJsonDocument doc(4096);
        deserializeJson(doc, jsonPayload);
        JsonObject obj = doc.as<JsonObject>();
        float percentageChange = obj["quoteResponse"]["result"][0]["regularMarketChangePercent"].as<float>();
        String tickString = 
            "$" + String(obj["quoteResponse"]["result"][0]["symbol"].as<char*>()) + ":" +
            String(obj["quoteResponse"]["result"][0]["regularMarketPrice"].as<float>(), 2) + 
            (percentageChange < 0 ? " (" : " (+")+
            String(percentageChange, 2) + "%)";

        retVal = tickString;
      }
    } else {
        retVal = httpClient.errorToString(httpCode);
    }

    httpClient.end();
    return retVal;
}