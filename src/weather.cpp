#include "weather.h"
#include <ArduinoJson.h>

#define WEATEHER_API_URL "https://www.el-tiempo.net/api/json/v2/provincias/%s/municipios/%s"


typedef struct SkyState
{
    int index; 
    const char* name;
};

SkyState gSkyStates[] = 
{
    {11, "Despejado"},
    {12, "Poco nuboso"},
    {13, "Intervalos nubosos"},
    {14, "Nuboso"},
    {16, "Muy nuboso"},
    {16, "Cubierto"},
    {17, "Nubes altas"},
    {23, "Intervalos nubosos con lluvia"},
    {24, "Nuboso con lluvia"},
    {25, "Muy nuboso con lluvia"},
    {26, "Cubierto con lluvia"},
    {33, "Intervalos nubosos con nieve"},
    {34, "Nuboso con nieve"},
    {35, "Muy nuboso con nieve"},
    {36, "Cubierto con nieve"},
    {43, "Intervalos nubosos con lluvia escasa"},
    {44, "Nuboso con lluvia escasa"},
    {45, "Muy nuboso con lluvia escasa"},
    {46, "Cubierto con lluvia escasa"},
    {51, "Intervalos nubosos con tormenta"},
    {52, "Nuboso con tormenta"},
    {53, "Muy nuboso con tormenta"},
    {54, "Cubierto con tormenta"},
    {61, "Intervalos nubosos con tormenta y lluvia escasa "},
    {62, "Nuboso con tormenta y lluvia escasa"},
    {63, "Muy nuboso con tormenta y lluvia escasa "},
    {64, "Cubierto con tormenta y lluvia escasa"},
    {71, "Intervalos nubosos con nieve escasa "},
    {72, "Nuboso con nieve escasa"},
    {73, "Muy nuboso con nieve escasa"},
    {74, "Cubierto con nieve escasa"},
    {99, "Error"}
};

const char* GetSkyStateName(int index)
{
    for (size_t i = 0; gSkyStates[i].index != 99; i++)
    {
        if(gSkyStates[i].index == index)
            return gSkyStates[i].name;
    }

    return "desconocido";
    
}

String Weather::FetchData(HTTPClient& httpClient, String province, String city)
{
    char urlBuffer[256];
    sprintf(urlBuffer, WEATEHER_API_URL, province.c_str(), city.c_str());

    httpClient.begin(urlBuffer); //HTTP

    int httpCode = httpClient.GET();
    String retVal;
    if (httpCode > 0) {
      if (httpCode == HTTP_CODE_OK) {
        String jsonPayload = httpClient.getString();
        DynamicJsonDocument doc(64*1024);
        deserializeJson(doc, jsonPayload);
        JsonObject obj = doc.as<JsonObject>();
        retVal = String("Hoy Cielo ") + obj["stateSky"]["description"].as<char*>() + 
        String(" Temp. ") + obj["temperatura_actual"].as<char*>() +
        String(" grados (") + obj["temperaturas"]["min"].as<char*>() + "-"+ obj["temperaturas"]["max"].as<char*>() + ")";

        int worstSkyTomorrow = 0;
        const auto& tomorrowSkies = obj["pronostico"]["manana"]["estado_cielo"];
        for (size_t i = 0; i < tomorrowSkies.size(); i++)
        {
            int value = atoi(tomorrowSkies[i].as<char*>());
            if(value <= 73)
                worstSkyTomorrow = max(worstSkyTomorrow, value);
        }

        int minTempTomorrow = 100;
        int maxTempTomorrow = -100;
        const auto& tomorrowTemp = obj["pronostico"]["manana"]["temperatura"];
        for (size_t i = 0; i < tomorrowTemp.size(); i++)
        {
            int value = atoi(tomorrowTemp[i].as<char*>());
            maxTempTomorrow = max(maxTempTomorrow, value);
            minTempTomorrow = min(minTempTomorrow, value);
        }

        
        retVal += " Manana Cielo" + String(GetSkyStateName(worstSkyTomorrow)) + 
        " Temp. " + String(minTempTomorrow) + "-" + String(maxTempTomorrow) + " grados";
      }
    } else {
        retVal = httpClient.errorToString(httpCode);
    }

    httpClient.end();
    return retVal;

}
