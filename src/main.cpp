
/*********************************************************************
This is an example for our Monochrome OLEDs based on SSD1306 drivers

  Pick one up today in the adafruit shop!
  ------> http://www.adafruit.com/category/63_98

This example is for a 64x48 size display using I2C to communicate
3 pins are required to interface (2 I2C and one reset)

Adafruit invests time and resources providing this open source code,
please support Adafruit and open-source hardware by purchasing
products from Adafruit!

Written by Limor Fried/Ladyada  for Adafruit Industries.
BSD license, check license.txt for more information
All text above, and the splash screen must be included in any redistribution
*********************************************************************/

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_I2CDevice.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <ArduinoJson.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include "ticker.h"
#include "stocks.h"
#include "crypto.h"
#include "weather.h"
  
#define OLED_RESET 0  // GPIO0
Adafruit_SSD1306 display(OLED_RESET);

#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2


#define LOGO16_GLCD_HEIGHT 16
#define LOGO16_GLCD_WIDTH  16

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

const char* ssid     = "MIWIFI_2G_7MYA";
const char* password = "eTAjWv2j";

Ticker mTicker;

void scanNetworks()
{
       int n = WiFi.scanNetworks();
   
  display.println("Scan done ");

   display.display();
  delay(2000);
  
  if (n == 0) {
        display.println("no networks found");
    } else {
        display.print(n);
        display.println(" networks found");
        display.display();
        delay(2000);
        display.clearDisplay();

        for (int i = 0; i < n; ++i) {
            // Print SSID and RSSI for each network found
            display.print(i + 1);
            display.print(": ");
            display.print(WiFi.SSID(i));
            display.print(" (");
            display.print(WiFi.RSSI(i));
            display.print(")");
            display.println((WiFi.encryptionType(i) == WIFI_AUTH_OPEN)?" ":"*");
               display.display();
            delay(2000);
          display.clearDisplay();
        }
    }
  display.clearDisplay(); 
}

void setup()   {
  Serial.begin(115200);

  Serial.println("Starting.");  

  mTicker.Init();
  

    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
  // Start I2C Communication SDA = 5 and SCL = 4 on Wemos Lolin32 ESP32 with built-in SSD1306 OLED
  Wire.begin(5, 4);

  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);

  display.clearDisplay();

  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.setTextSize(1);

    display.println("Conectando a: ");
    display.println(ssid);
    display.display();
    
    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) 
    {
        delay(500);
        Serial.print(".");
        display.display();
    }

    display.println("Conectado!");
    display.print("IP : ");
    display.println(WiFi.localIP());
    display.display();
}

typedef enum TickerType
{
  TickerType_Stocks,
  TickerType_Weather,
  TickerType_Crypto
};

typedef struct TickerQueueItem
{
  TickerType type;
  String param1;
  String param2;
};

TickerQueueItem tickerQueueItems[] =
{
//  {TickerType_Weather,"08","08281"},      // Teia, Barcelona
  {TickerType_Crypto,"ADA",""},
  {TickerType_Stocks,"GME",""},
  {TickerType_Stocks,"UBI.PA",""},
};

const int MAX_QUEUE_ITEMS = 3;
int currentQueueItem = 0;

void loop() {
  mTicker.Update(20);
  delay(20);

  if(!mTicker.FinishedScrolling())
  {
    return;
  }

  HTTPClient http;

  switch(tickerQueueItems[currentQueueItem].type)
  {
    case TickerType_Stocks:
    {
      Stocks stocks;
      mTicker.SetMessage(stocks.FetchData(http, tickerQueueItems[currentQueueItem].param1).c_str());
    }
    break;
    case TickerType_Crypto:
    {
      Crypto crypto;
      mTicker.SetMessage(crypto.FetchData(http, tickerQueueItems[currentQueueItem].param1).c_str());
    }
    break;
    case TickerType_Weather:
    {
      Weather weather;
      mTicker.SetMessage(weather.FetchData(http, tickerQueueItems[currentQueueItem].param1, tickerQueueItems[currentQueueItem].param2).c_str());
  
    }
    break;

  }



  currentQueueItem = (currentQueueItem + 1) % MAX_QUEUE_ITEMS;
}
