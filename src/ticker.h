#pragma once

#include <LedMatrix.h>


class Ticker
{
public:
    static const int MAX_MESSAGE_LIST_SIZE = 16;

    static const int NUMBER_OF_DEVICES = 4;
    static const int CS_PIN = 15;
    static const int CLK_PIN = 14;
    static const int MISO_PIN = 2;
    static const int MOSI_PIN = 12;

    typedef struct Message
    {
        static const int MAX_MESSAGE_SIZE = 64;

        bool mScroll = false;
        int mDisplayTime = 0;
        char mMessage[MAX_MESSAGE_SIZE] = {};
    }LedMatrixMessage;


    Ticker() :
    mLedMatrix(NUMBER_OF_DEVICES, CLK_PIN, MISO_PIN, MOSI_PIN, CS_PIN),
    mCurrentMessageIndex(0),
    mTopMessageIndex(0)
    {};

    void Init();
    void Update(unsigned int deltams);

    void NextMessage();

    void PushMessage(char* message, unsigned int displayTime, bool scroll);
    bool HasPendingMessages() const { return mTopMessageIndex != mCurrentMessageIndex; };

    void SetMessage(const char* message);

    bool FinishedScrolling() { return mLedMatrix.IsEndOfScroll(); }
private:
    void UpdateText(unsigned int deltams);
    LedMatrix mLedMatrix;

    LedMatrixMessage mMessageList[MAX_MESSAGE_LIST_SIZE];
    int mCurrentMessageIndex;
    int mTopMessageIndex;

    bool mUseMessageQueue = false;

};